package com.example.user.service.service;

import java.util.List;

import com.example.user.service.dto.ResponseDto;
import com.example.user.service.dto.UserDto;
import com.example.user.service.entity.User;

public interface UserService {
	
	ResponseDto addUser(UserDto userDto);
	
	List<User> getUsers();
	
	UserDto getUser(int id);

}
