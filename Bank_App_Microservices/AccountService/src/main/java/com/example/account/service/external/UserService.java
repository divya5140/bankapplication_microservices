package com.example.account.service.external;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.account.service.configuration.FeignConfiguration;
import com.example.account.service.dto.UserDto;


@FeignClient(name="user-service",url="http://localhost:8081/api/v1",configuration = FeignConfiguration.class)
public interface UserService {
	
	@GetMapping("/users/{id}")
	public ResponseEntity<UserDto> getUser(@PathVariable int id);

}
