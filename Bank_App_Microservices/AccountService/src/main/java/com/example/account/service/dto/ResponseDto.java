package com.example.account.service.dto;

public record ResponseDto(String message) {
}