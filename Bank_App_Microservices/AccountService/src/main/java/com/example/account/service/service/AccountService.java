package com.example.account.service.service;

import com.example.account.service.dto.AccountDto;
import com.example.account.service.dto.ResponseDto;

public interface AccountService {
	
	ResponseDto createAccount(AccountDto accountDto);

}
