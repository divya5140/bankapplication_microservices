package com.example.account.service.dto;

public record AccountDto(int accountId,

		long accountNumber,

		String accounttype,

		double balance,

		int userId) {

}
